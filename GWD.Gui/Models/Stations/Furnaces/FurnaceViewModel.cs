﻿
namespace GWD.Gui.Models.Stations.Furnaces
{
    public class FurnaceViewModel
    {
        public FurnaceViewModel() { }

        public FurnaceViewModel(GWD.Models.Lab.StationModel stationSummaryModel)
        {
            this.StationSummaryModel = stationSummaryModel;
            this.SetPoint = 50;
            this.RampRate = 5.0F;
        }

        public GWD.Models.Lab.StationModel StationSummaryModel { get; set; }


        public int SetPoint { get; set; }

        public float RampRate { get; set; }
    }
}
