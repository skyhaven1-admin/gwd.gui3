﻿using System.Threading.Tasks;

namespace GWD.Gui.Services
{
    public class ApiService : Interfaces.IApiService
    {

        private readonly Microsoft.Extensions.Configuration.IConfiguration _config;

        private readonly string _apiUrl;
        private readonly string _robotUrl;

        public ApiService(
            Microsoft.Extensions.Configuration.IConfiguration config
            )
        {
            _config = config;
            _apiUrl = _config["ApiRootUrl"] + @"/api/";
            _robotUrl = _config["RobotTempUrl"] + @"/api/";
        }

        public async Task<T> GetApiObject<T>(string objectOrUrl, bool isApi = true)
        {
            //temp solution - one method, two urls
            string apiUrl = (isApi ? _apiUrl : _robotUrl);

            //var url = Microsoft.AspNetCore.WebUtilities.QueryHelpers.AddQueryString(_apiUrl, "lab", "1");

            var url = apiUrl + objectOrUrl;//may need to make this more sophisticated later
            T model = default(T);

            //build some kind of a framework around these APi requests later, just get it for now
            using (var httpClient = new System.Net.Http.HttpClient())
            {
                using (var response = await httpClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        model = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(apiResponse);
                    } else
                    {
                        throw new System.Exception("Error thrown by API");
                    }
                }

            }


            return model;
        }

        public async Task<bool> PostApiObject(string objectOrUrl, object postModel)
        {
            var url = _apiUrl + objectOrUrl;//may need to make this more sophisticated later

            System.Net.Http.StringContent content = new System.Net.Http.StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postModel), System.Text.Encoding.UTF8, "application/json");


            using (var httpClient = new System.Net.Http.HttpClient())
            {
                using (var response = await httpClient.PostAsync(url, content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(apiResponse);
                }
            }
        }


    }
}
