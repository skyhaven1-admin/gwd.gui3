﻿using System.Threading.Tasks;

namespace GWD.Gui.Services.Interfaces
{
    public interface IApiService
    {
        Task<T> GetApiObject<T>(string objectOrUrl, bool isApi = true);
        Task<bool> PostApiObject(string objectOrUrl, object postModel);
    }
}
