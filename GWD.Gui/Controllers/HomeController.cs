﻿using GWD.Gui.Models;
using GWD.Models.Lab.RobotCommands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GWD.Gui.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Services.Interfaces.IApiService _apiService;

        public HomeController(
            ILogger<HomeController> logger,
            Services.Interfaces.IApiService apiService
            )
        {
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index()
        {
            //for now, this uses the same API method as the lab tests overview
            Task<GWD.Models.Lab.LabOverviewModel> getStringTask = _apiService.GetApiObject<GWD.Models.Lab.LabOverviewModel>("Lab");
            var labOverview = await getStringTask;

            return View(labOverview);
        }

        public async Task<IActionResult> LabTests()
        {
            Task<GWD.Models.Lab.LabOverviewModel> getStringTask = _apiService.GetApiObject<GWD.Models.Lab.LabOverviewModel>("Lab");
            var labOverview = await getStringTask;

            return View(labOverview);
        }

        public async Task<IActionResult> WorkflowTests()
        {
            //temp - get the robot and stations:
            Task<GWD.Models.Lab.LabOverviewModel> getLabTask = _apiService.GetApiObject<GWD.Models.Lab.LabOverviewModel>("Lab");
            var labOverview = await getLabTask;

            ///api/Robot/GetRobotName
            Task<string> getRobotTask = _apiService.GetApiObject<string>("Robot/GetRobotName", false);
            var robotName = await getRobotTask;
            ViewBag.RobotName = robotName;

            return View(labOverview);
        }


        public async Task<IActionResult> WorkflowTests2()
        {

            //we are only going to operate on one robot - set live robot here or dummy if just testing

            //temp - get the robot and stations:
            Task<GWD.Models.Lab.LabOverviewModel> getLabTask = _apiService.GetApiObject<GWD.Models.Lab.LabOverviewModel>("Lab");
            var labOverview = await getLabTask;

            var robotUsing = labOverview.Robots.First(x => x.RobotTypeEnum == GWD.Models.Enums.RobotType.Dummy);
            ViewBag.RobotId = robotUsing.RobotId;
            ViewBag.RobotName = robotUsing.RobotName;

            return View(labOverview);
        }


        #region Robot Actions



        /// <summary>
        /// Move to station - goes via API, no direct connection to robot
        /// </summary>
        /// <param name="robotaction"></param>
        /// <param name="robotid"></param>
        /// <param name="stationenum"></param>
        /// <returns></returns>
        public async Task<IActionResult> MoveToStation(int robotid, int stationid)
        {
            var model = new MoveToStationCommandModel()
            {
                RobotId = robotid,
                StationId = stationid
            };

            Task<bool> getRobotTask = _apiService.PostApiObject("Commands/MoveRobotToStationAndPrepareToOperate", model);
            var result = await getRobotTask;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }



        [HttpPost]
        public async Task<IActionResult> DispenseCurrentSolidIntoBoat(int robotid, int boatIndex, float targetMg, bool tapBeforeDosing, bool tapWhileDosing, int tappingIntensity)
        {
            var model = new DispenseCurrentSolidModel()
            {
                RobotId = robotid,
                BoatIndex = boatIndex,

                TapBeforeDosing = tapBeforeDosing,
                TapWhileDosing = tapWhileDosing,
                TappingIntensity = tappingIntensity,
                TargetAmountMilligram = targetMg
            };

            Task<bool> dispenseTask = _apiService.PostApiObject("Commands/DispenseCurrentSolidIntoBoat", model);
            var result = await dispenseTask;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }


        public async Task<IActionResult> TransferSolidsToJar(int robotid)
        {

            Task<bool> dispenseTask = _apiService.GetApiObject<bool>(@"Commands/TransferSolidToBallMillJar/" + robotid);
            var result = await dispenseTask;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }


        [HttpPost]
        public async Task<IActionResult> PutBoatBackInStorage(int robotid, int boatIndex)
        {
            var model = new PutBoatBackInStorageCommandModel()
            {
                RobotId = robotid,
                BoatIndex = boatIndex
            };

            Task<bool> boatTask = _apiService.PostApiObject("Commands/PutBoatBackInStorage", model);
            var result = await boatTask;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }


        [HttpPost]
        public async Task<IActionResult> PutSolidIntoDispenser(int robotid, int solidIndex)
        {
            var model = new PutSolidIntoDispenserCommandModel()
            {
                RobotId = robotid,
                SolidIndex = solidIndex
            };

            Task<bool> task = _apiService.PostApiObject("Commands/PutSolidIntoDispenser", model);
            var result = await task;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }




        [HttpPost]
        public async Task<IActionResult> ReturnSolidToHotel(int robotid, int solidIndex)
        {
            var model = new ReturnSolidToHotelCommandModel()
            {
                RobotId = robotid,
                SolidIndex = solidIndex
            };

            Task<bool> task = _apiService.PostApiObject("Commands/ReturnSolidToHotel", model);
            var result = await task;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }



        /// <summary>
        /// Obviosuly need to do this with strongly typed models - fine for now
        /// Models will need to be flexibly types driven by meta data
        /// </summary>
        /// <param name="robotid"></param>
        /// <param name="boatIndex"></param>
        /// <param name="targetMg"></param>
        /// <param name="tapBeforeDosing"></param>
        /// <param name="tapWhileDosing"></param>
        /// <param name="tappingIntensity"></param>
        /// <param name="solidIndex"></param>
        /// <param name="stationId"></param>
        /// <param name="stationIdAfterwards"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> TestCompositeTask(int robotid, int boatIndex, float targetMg, bool tapBeforeDosing, bool tapWhileDosing, int tappingIntensity,
            int solidIndex, int stationId, int stationIdAfterwards)
        {
            var model = new TestCompositeTaskModel()
            {
                RobotId = robotid,
                BoatIndex = boatIndex,

                TapBeforeDosing = tapBeforeDosing,
                TapWhileDosing = tapWhileDosing,
                TappingIntensity = tappingIntensity,
                TargetAmountMilligram = targetMg,
                SolidIndex = solidIndex,
                StationId = stationId,
                StationIdAfterwards = stationIdAfterwards
            };

            Task<bool> dispenseTask = _apiService.PostApiObject("Commands/TestCompositeTask", model);
            var result = await dispenseTask;

            TempData["RobotTaskOutcome"] = result;

            return RedirectToAction("WorkflowTests2");
        }















        //////////////////OLDER STUFF

        /// <summary>
        /// set which robot to use (dummy or not) will fail if not connected - uses robot service - needs to be removed later
        /// </summary>
        /// <param name="isdummy"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetRobot(bool isdummy)
        {
            ////api/Robot/InstantiateRobots?dummy=true
            Task<bool> getRobotTask = _apiService.GetApiObject<bool>("Robot/InstantiateRobots?dummy=" + isdummy, false);
            var robotName = await getRobotTask;

            return RedirectToAction("WorkflowTests");
        }



        /// <summary>
        /// posted command to robot with a prarm
        /// </summary>
        /// <param name="indexparam"></param>
        /// <param name="robotaction"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> RobotAction(int indexparam, string robotaction)
        {
            ////api/Robot/InstantiateRobots?dummy=true
            Task<bool> getRobotTask = _apiService.GetApiObject<bool>("Robot/" + robotaction + "?index=" + indexparam, false);
            var robotTask = await getRobotTask;

            TempData["RobotTaskOutcome"] = robotTask;

            return RedirectToAction("WorkflowTests");
        }

        [HttpGet]
        public async Task<IActionResult> RobotActionNoParams(string robotaction)
        {
            ////api/Robot/InstantiateRobots?dummy=true
            Task<bool> getRobotTask = _apiService.GetApiObject<bool>("Robot/" + robotaction, false);
            var robotTask = await getRobotTask;

            TempData["RobotTaskOutcome"] = robotTask;

            return RedirectToAction("WorkflowTests");
        }

        #endregion

        #region stations


        [HttpGet]
        public async Task<IActionResult> SolidDispenserStandardAction(string dispenseraction, int dispenserid)
        {
            var url = string.Format("SolidDispenser/{0}/{1}", dispenseraction, dispenserid);
            ////api/Robot/InstantiateRobots?dummy=true
            Task<bool> getStationTask = _apiService.GetApiObject<bool>(url, true);
            var stationTask = await getStationTask;

            TempData["StationTaskOutcome"] = stationTask;

            return RedirectToAction("WorkflowTests");
        }

        #endregion


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
