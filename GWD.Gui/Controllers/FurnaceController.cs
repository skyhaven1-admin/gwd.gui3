﻿using GWD.Gui.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GWD.Gui.Controllers
{
    public class FurnaceController : BaseController
    {

        private readonly ILogger<FurnaceController> _logger;
        private readonly Services.Interfaces.IApiService _apiService;

        public FurnaceController(
            ILogger<FurnaceController> logger,
            Services.Interfaces.IApiService apiService
            )
            : base()
        {
            _logger = logger;
            _apiService = apiService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetSetpoint(GWD.Gui.Models.Stations.Furnaces.FurnaceViewModel model)
        {
            var postModel = new GWD.Models.Lab.Furnace.SetSetpointModel()
            {
                StationId = model.StationSummaryModel.StationId,
                SetPoint = model.SetPoint
            };

            var postSetpointTask = _apiService.PostApiObject("Furnace/Setpoint/", postModel);
            var success = await postSetpointTask;

            if (success)
            {
                TempData["TempResultMessageGreen"] = "Operation was successful";
            }
            else
            {
                TempData["TempResultMessageRed"] = "Operation failed";
            }
            return RedirectToAction("LabTests", "home");
        }

        [HttpGet]
        public async Task<IActionResult> ReadSetpoint(int sid)
        {

            Task<int> getSetpointTask = _apiService.GetApiObject<int>("Furnace/Setpoint/" + sid);
            var temp = await getSetpointTask;
            if (temp > 0)
            {
                TempData["TempResultMessageGreen"] = "Temperature found to be " + temp;
            }
            else
            {
                TempData["TempResultMessageRed"] = "Operation failed";
            }
            return RedirectToAction("LabTests", "home");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetRampRate(GWD.Gui.Models.Stations.Furnaces.FurnaceViewModel model)
        {


            var postModel = new GWD.Models.Lab.Furnace.SetRampRateModel()
            {
                StationId = model.StationSummaryModel.StationId,
                Rate = model.RampRate
            };

            var postSetpointTask = _apiService.PostApiObject("Furnace/Ramprate/", postModel);
            var success = await postSetpointTask;

            if (success)
            {
                TempData["TempResultMessageGreen"] = "Operation was successful";
            }
            else
            {
                TempData["TempResultMessageRed"] = "Operation failed";
            }
            return RedirectToAction("LabTests", "home");
        }

        [HttpGet]
        public async Task<IActionResult> ReadRampRate(int sid)
        {
            Task<int> getRamprateTask = _apiService.GetApiObject<int>("Furnace/Ramprate/" + sid);
            var rate = await getRamprateTask;
            if (rate > 0)
            {
                TempData["TempResultMessageGreen"] = "Rate found to be " + rate;
            }
            else
            {
                TempData["TempResultMessageRed"] = "Operation failed";
            }
            return RedirectToAction("LabTests", "home");
        }

        [HttpGet]
        public async Task<IActionResult> ReadCurrentTemp(int sid)
        {

            Task<int> getRamprateTask = _apiService.GetApiObject<int>("Furnace/Temperature/" + sid);
            var temp = await getRamprateTask;
            if (temp > 0)
            {
                TempData["TempResultMessageGreen"] = "Temp found to be " + temp;
            }
            else
            {
                TempData["TempResultMessageRed"] = "Operation failed";
            }
            return RedirectToAction("LabTests", "home");

         
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RunTest(GWD.Gui.Models.Stations.Furnaces.FurnaceViewModel model)
        {
            var postModel = new GWD.Models.Lab.Furnace.SetSetpointModel()
            {
                StationId = model.StationSummaryModel.StationId,
                SetPoint = model.SetPoint
            };

            var postSetpointTask = _apiService.PostApiObject("Furnace/RunTest/", postModel);
            var success = await postSetpointTask;

            if (success)
            {
                TempData["TempResultMessageGreen"] = "Test was successful";
            }
            else
            {
                TempData["TempResultMessageRed"] = "Test failed";
            }
            return RedirectToAction("LabTests", "home");
        }

        
    }
}
